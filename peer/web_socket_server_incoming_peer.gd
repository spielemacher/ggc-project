class_name WebSocketServerIncomingPeer
extends Resource


enum STATUS {
	FAILED,
	PENDING,
	SUCCESS,
}


var connect_time: int
var tls_options: TLSOptions
var peertcp: StreamPeerTCP
var peertls: StreamPeerTLS
var websocket: WebSocketPeer


func _init(new_peer: StreamPeerTCP, key: CryptoKey, cert: X509Certificate) -> void:
	connect_time = Time.get_ticks_msec()
	tls_options = TLSOptions.server(key, cert)
	peertcp = new_peer
	peertls = null
	websocket = null


func poll() -> STATUS:
	if is_instance_valid(websocket):
		websocket.poll()
		match websocket.get_ready_state():
			WebSocketPeer.STATE_CONNECTING, WebSocketPeer.STATE_CLOSING:
				return STATUS.PENDING
			WebSocketPeer.STATE_OPEN:
				return STATUS.SUCCESS
		return STATUS.FAILED # <- WebSocketPeer.STATE_CLOSED
	if is_instance_valid(peertls):
		peertls.poll()
		match peertls.get_status():
			StreamPeerTLS.STATUS_HANDSHAKING:
				return STATUS.PENDING
			StreamPeerTLS.STATUS_CONNECTED:
				websocket = WebSocketPeer.new()
				websocket.accept_stream(peertls)
				return STATUS.PENDING
		return STATUS.FAILED # StreamPeerTLS.STATUS_CONNECTED/ERROR*
	peertcp.poll()
	match peertcp.get_status():
		StreamPeerTCP.STATUS_CONNECTING:
			return STATUS.PENDING
		StreamPeerTCP.STATUS_CONNECTED:
			peertls = StreamPeerTLS.new()
			peertls.accept_stream(peertcp, tls_options)
			return STATUS.PENDING
	return STATUS.FAILED # StreamPeerTCP.STATUS_NONE/ERROR

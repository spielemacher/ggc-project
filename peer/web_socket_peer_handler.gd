class_name WebSocketPeerHandler
extends Node

signal connecting
signal connected
signal disconnected(code: int, reason: String)
signal packets_received(packets: int)
signal data_received(packet_data: PackedByteArray, was_string: bool)

var last_state: WebSocketPeer.State = WebSocketPeer.STATE_CLOSED

var peer: WebSocketPeer = null:
	set(new_peer):
		peer = new_peer
		if is_instance_valid(peer):
			last_state = peer.get_ready_state()
			set_process(true)
			return
		last_state = WebSocketPeer.STATE_CLOSED
		set_process(true)


func _ready() -> void:
	set_process(is_instance_valid(peer))


func _process(_delta: float) -> void:
	assert(is_instance_valid(peer))
	peer.poll()
	match peer.get_ready_state():
		WebSocketPeer.STATE_CONNECTING:
			if last_state != WebSocketPeer.STATE_CONNECTING:
				connecting.emit()
		WebSocketPeer.STATE_OPEN:
			if last_state != WebSocketPeer.STATE_OPEN:
				connected.emit()
			handle_open()
		WebSocketPeer.STATE_CLOSING:
			pass # mh ... :/
		WebSocketPeer.STATE_CLOSED:
			if last_state != WebSocketPeer.STATE_CLOSED:
				disconnected.emit(peer.get_close_code(), peer.get_close_reason())
	last_state = peer.get_ready_state()


func handle_open() -> void:
	var packets: int = peer.get_available_packet_count()
	if packets > 0:
		packets_received.emit(packets)
	while peer.get_available_packet_count() > 0:
		data_received.emit(peer.get_packet(), peer.was_string_packet())
	

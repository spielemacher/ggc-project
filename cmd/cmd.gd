extends Node

signal execution_finished
signal cmd_print(text: String)


@export var welcome: String = "Welcome to GGC-Server"


var commands: Dictionary = {
	"quit": {
		"method": quit,
		"arguments": 0},
	"add": {
		"method": add,
		"arguments": 2},
}
var shutdown: bool = false
var handler: CMDHandler = null


func command(cmd: String) -> void:
	var words: PackedStringArray = cmd.split(" ")
	if words.size() == 0:
		execution_finished.emit()
		return
	var com: String = words[0]
	if not com in commands:
		handler.add_text("Unknown command '%s'" % words[0])
		execution_finished.emit()
		return
	words.remove_at(0)
	if not words.size() == commands[com]["arguments"]:
		handler.add_text("%s: %d arguments provided, but %d expected" % [com, words.size(), commands[com]["arguments"]])
		execution_finished.emit()
		return
	callv(com, words)
	execution_finished.emit()


func quit() -> void:
	shutdown = true
	
	# here it should wait for tasks to finish!
	
	get_tree().call_deferred("quit")
	handler.add_text("quitting...")


# Just to test the cmd
func add(a: String, b: String) -> void:
	if not a.is_valid_float():
		handler.add_text("Argument 1 is not a number")
		return
	if not b.is_valid_float():
		handler.add_text("Argument 2 is not a number")
		return
	var a_number: float = a.to_float()
	var b_number: float = b.to_float()
	handler.add_text("%f + %f = %f" % [a_number, b_number, a_number + b_number])
